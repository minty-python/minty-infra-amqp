.. _readme:

Introduction
============

Minty AMQP infrastructure

Getting started
---------------

n/a

More documentation
------------------

Please see the generated documentation via CI for more information about this
module and how to contribute in our online documentation. Open index.html
when you get there:
`<https://gitlab.com/minty-python/minty-infra-amqp/-/jobs/artifacts/master/browse/tmp/docs?job=qa>`_


Contributing
------------

Please read `CONTRIBUTING.md <https://gitlab.com/minty-python/minty-infra-amqp/blob/master/CONTRIBUTING.md>`_
for details on our code of conduct, and the process for submitting pull requests to us.

Versioning
----------

We use `SemVer <https://semver.org/>`_ for versioning. For the versions
available, see the
`tags on this repository <https://gitlab.com/minty-python/minty-infra-amqp/tags/>`_

License
-------

Copyright (c) Minty Team and all persons listed in the file `CONTRIBUTORS`

This project is licensed under the EUPL, v1.2. See the `EUPL-1.2.txt` in the
`LICENSES` directory for details.

.. SPDX-FileCopyrightText: 2020 Mintlab B.V.
..
.. SPDX-License-Identifier: EUPL-1.2
